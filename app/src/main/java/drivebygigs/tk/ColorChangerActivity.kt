package drivebygigs.tk

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.database.Observable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_color_changer.*
import java.util.*

class ColorChangerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_changer)
        val timeChangedViewModel=ViewModelProviders.of(this).get(TimeChangedViewMode::class.java)
        val  calendar=Calendar.getInstance()

    }


}



class TimeChangedViewMode:ViewModel(){

    val timerValue = MutableLiveData<Long>()

    init{
        timerValue.value=System.currentTimeMillis()
    }

}
