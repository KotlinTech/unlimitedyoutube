package drivebygigs.tk

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

const val YOUTUBE_VIDEO_ID="9aIoz_6ZqOI"
class MainActivity : YouTubeBaseActivity(),YouTubePlayer.OnInitializedListener  {

    val TAG="MAINACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layout=layoutInflater.inflate(R.layout.activity_main,null)as ConstraintLayout
        setContentView(layout)

        val playerView=YouTubePlayerView(this)
        playerView.layoutParams=ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        layout.addView(playerView)

        playerView.initialize(getString(R.string.GOOGLE_API_KEY),this)

    }

    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
     Log.d(TAG,"onInitializationSuccess:provider is ${p0?.javaClass}")
     Log.d(TAG,"onInitializationSuccess:youtube player is ${p1?.javaClass}")
        Toast.makeText(this,"Youtube Successfully",Toast.LENGTH_LONG).show()

        if(!p2){
            p1?.loadVideo(YOUTUBE_VIDEO_ID)
        }else{
            p1?.play()
        }
     }

    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
      val REQUEST_CODE=0

        if(p1?.isUserRecoverableError==true){
            p1.getErrorDialog(this,REQUEST_CODE).show()
        }else{
            val errorMessage="There was an error initializing the youtubePlayer ($p1)"
            Toast.makeText(this,errorMessage,Toast.LENGTH_LONG).show()
        }
    }

    val playBackListener=object :YouTubePlayer.PlaybackEventListener{
        override fun onSeekTo(p0: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onBuffering(p0: Boolean) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onPlaying() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onStopped() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onPaused() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }
}
